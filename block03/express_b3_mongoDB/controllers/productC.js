const Products = require('../models/productM');

class ProductController {

    // ADD PRODUCT
    async add (req, res) {
        console.log("Product added!")
        let { categoryID, name, price, color, description } = req.body;
        try {
            const addProduct = await Products.create({ 
                name: name,
                price: price,
                color: color,
                description: description,
                categoryID: categoryID,
            });
            res.send(addProduct)
        }
        catch(error){
            res.send({ error })
        }
    }
    // DELETE PRODUCT
    async delete (req, res){
        console.log("Product deleted!")
        let { _id } = req.body;
        try {
            const removeProduct = await Products.deleteOne({ _id });
            res.send({ removeProduct });
        }
        catch(error){
            res.send({ error });
        };
    }
    // UPDATE PRODUCT
    async update (req, res){
        console.log("Product updated!")
        let { _id, name, price, color, description, newName, newColor, newPrice, newDes } = req.body;
        try {
            const updateProduct = await Products.updateOne(
                { _id, name, price, color, description },{ 
                    name: newName,
                    price: newPrice,
                    color: newColor,
                    description: newDes, 
                }
             );
            res.send({ updateProduct });
        }
        catch(error){
            res.send({ error });
        };
    }
      // GET ALL PRODUCTS
      async find (req, res){
        console.log("All products available!")
        try {
            const products = await Products.find({});
            res.send(products);
        }
        catch(error){
            res.send({ error })
        };
    }
    // GET ONE PRODUCT BY TITLE OR ID
    async findOne (req ,res){
        console.log("Product available!")
        let { product } = req.params;
        try {
            const oneProduct = await Products.findOne({ _id: product });
            res.send(oneProduct);
        }
        catch(error){
            res.send({ error })
        };

    }
    // GET ALL PRODUCTS BY CATEGORY
    async findAllProdsByCat (req ,res){
        console.log("All products available in category!")
        let { category } = req.params;
        try {
            const allProdCat = await Products.find({ categoryID: category });
            res.send(allProdCat);
        }
        catch(error){
            res.send({ error });
        };
    }
};
module.exports = new ProductController();