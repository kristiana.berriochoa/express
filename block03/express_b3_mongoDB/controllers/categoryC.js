const Categories = require('../models/categoriesM');

class CategoryController {
    
    // ADD CATEGORY
    async add (req, res){
        console.log("Category added!");
        let { category } = req.body;
        try {
            const addCategory = await Categories.create({ category });
            res.send(addCategory);
        }
        catch(error){
            res.send({ error });
        };
    }
    // DELETE CATEGORY
    async delete (req, res){
        console.log("Category deleted!");
        let { category } = req.body;
        try {
            const removeCategory = await Categories.deleteOne({ category });
            res.send({ removeCategory });
        }
        catch(error){
            res.send({ error });
        };
    }
    // UPDATE CATEGORY
    async update (req, res){
        console.log("Category updated!");
        let { category, newCategory } = req.body;
        try {
            const updateCategory = await Categories.updateOne(
                { category },{ category: newCategory }
            );
            res.send({ updateCategory });
        }
        catch(error){
            res.send({ error });
        };
    }
    // FIND ALL CATEGORIES
    async find (req, res){
        console.log("All categories available!");
        try {
            const categories = await Categories.find({});
            res.send(categories);
        }
        catch (error){
            res.send({ error });
        };
    }
     //GET ONE PRODUCT BY NAME OR ID
    async findOne (req ,res){
        console.log("Product available in category!");
        let { product } = req.params;
        try {
            const oneProduct = await Categories.findOne({ _id: product });
            res.send(oneProduct);
        }
        catch(error){
            res.send({ error })
        };
    }
   
};
module.exports = new CategoryController();

/*
    async findOne (req ,res){
        console.log("Product available in category!");
        let { product } = req.params;
        try {
            const oneProduct = await Categories.findOne({ name: product });
            res.send({ oneProduct });
        }
        catch(error){
            res.send({ error })
        };
    }
*/