const express = require('express') 
const router = express.Router()
const controller = require('../controllers/categoryC');

//example: /productsDB/categories/add;

// =============== ADD NEW CATEGORY TO DB ===============
router.post('/add', controller.add);

// =============== REMOVE ONE CATEGORY BY ID ===============
router.post('/delete', controller.delete);

// =============== UPDATE ONE CATEGORY BY ID ===============
router.post('/update', controller.update);

// =============== FIND ALL CATEGORIES ===============
router.get('/', controller.find);

// =============== FIND ONE PRODUCT BY NAME OR ID ===============
router.get('/:product', controller.findOne);

module.exports  = router;