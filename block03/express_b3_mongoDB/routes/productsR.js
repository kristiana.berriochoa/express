const express = require('express') 
const router = express.Router();
const controller = require('../controllers/productC');

// =============== ADD NEW PRODUCT TO DB ===============
router.post("/add", controller.add);

// =============== REMOVE ONE PRODUCT BY ID ===============
router.post("/delete", controller.delete);

// =============== UPDATE ONE PRODUCT BY ID ===============
router.post('/update', controller.update);

// =============== FIND ALL PRODUCTS ===============
router.get('/', controller.find);

// =============== FIND ONE PRODUCT BY TITLE OR ID ===============
router.get('/:product', controller.findOne);

// =============== FIND ALL PRODUCTS BY CATEGORY ID ===============
router.get('/prod/:category', controller.findAllProdsByCat);

module.exports =  router;