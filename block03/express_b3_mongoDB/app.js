// =============== REQUIRE AND RUN EXPRESS IN A SINGLE LINE
const app = require('express')();
// =============== ADD BODY PARSER ===============
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const port = process.env.port || 3001;
// =============== BODY PARSER SETTINGS =====================
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// =============== MONGO CONNECTION, CHECK IF DB IS RUNNING =====================
async function connecting(){
    try {
        await mongoose.connect('mongodb://127.0.0.1/productsDB', { useUnifiedTopology: true , useNewUrlParser: true })
        console.log('Connected to the DB')
    } catch ( error ) {
        console.log('ERROR: Seems like your DB is not running, please start it up !!!');
    }
}
connecting() // temp stuff to suppress internal warning of mongoose which would be updated by them soon
mongoose.set('useCreateIndex', true); // end connection to mongo and checking if DB is running
// =============== ROUTES ==============================
const categoriesR = require('./routes/categoriesR');
const productsR = require('./routes/productsR');
// =============== USE ROUTES ==============================
app.use("/categories", categoriesR);
app.use("/products", productsR);
// =============== START SERVER =====================
app.listen(port, () => 
    console.log(`server listening on port ${port}`
));