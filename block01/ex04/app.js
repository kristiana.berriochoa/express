const express = require('express');
const app = express ();
const port = 3001;

app.listen(port, () => {
    console.log(`server running on port ${port}`)
});

var languages = {
    NL: "Hallo wereld!",
    HI: "नमस्ते दुनिया!",
    FR: "Bonjour le monde!",
    ES: "Hola mundo!",
    IT: "Ciao mondo!",
    CH: "你好，世界!",
    JP: "こんにちは世界!",
    AR: "مرحبا بالعالم!",
    EN: "Hello world!",
};

app.get('/', (req , res ) => {
    res.send(`<h1>Hello world!</h1>`)
});

app.get("/:language", (req, res) => {
    var lang = req.params.language;
    if(lang in languages){
        res.send(`<h1>${languages[lang]}</h1>`)
    } else {
        res.send(`<h1>Hello world in ${lang} not found!</h1>`)
    }
});

app.get("/:language/remove", (req, res) => {
    const { language } = req.params;
    delete language[language];
    res.send(`<h1>${language} removed<h1>`);
}); 

app.get("/:language/update/:newPhrase", (req, res) => {
    const { language, newPhrase } = req.params;
    if(language in languages){
        var oldPhrase = languages[language];
        languages[language] = newPhrase.replace("_", " ");
        res.send(`<h1>${language} updated from ${oldPhrase} to ${newPhrase.replace("_", " ")}</h1>`) 
    }
});

app.get("/:language/:phrase", (req, res) => {
    const { language, phrase } = req.params; // New language and new phrase
    if(language in languages){ // Check to see if new language exists as key in object 
        res.send(`<h1> Action fobidden, ${language} is already present in the system!</h1>`) // If yes, send following message
    } else { // If not, add language
        var newPhrase = phrase.replace("_", " ") // Use replace() method to change "_" to " " and save to new variable
        languages[language] = newPhrase; // Add language and phrase to the object
        res.send(`<h1>${language} added with message ${newPhrase}</h1>`) // Send following message
    }
});

