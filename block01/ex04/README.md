## EXERCISE 4:

Extend the previous exercise and add the possibility to update a language.

Example of usage:
    > URL Message
    > /DE Hello World in DE not found
    > /DE/HalloWelt DE added with message "HalloWelt"
    > /DE HalloWelt
    > /DE/Hallo_Welt Action fobidden, DE is already present in the system
    > /DE/update/Hallo_Welt DE updated from "HalloWelt" to "Hallo Welt"

Note: by completing this exercise you will have your first CRUD app! Congratulations!

***Your solution goes to ex04 folder***