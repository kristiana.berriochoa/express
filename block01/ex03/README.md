## EXERCISE 3:

Extend the previous exercise and add the possibility to remove a language. In fact let's make the app start with no languages saved in memory.

Example of usage:
    > URL Message
    > /DE Hello World in DE not found
    > /DE/Hallo_Welt DE added with message "Hallo Welt"
    > /DE Hallo Welt
    > /DE/remove DE removed
    > /DE Hello World in DE not found

***Your solution goes to ex03 folder***