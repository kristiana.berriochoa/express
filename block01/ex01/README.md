## EXERCISE 1:

Write a multylanguage HelloWorld app with Express. 
To do so you need to use req.params property to get the lanaguage the user wants the message to be printed in. 
Make sure that if the URL does not specify any language then the app will default to English. 
For this exercise and all the next to come your app will listen to localhost:3001 instead of checking for a file in a folder like we have done so far.

Example of usage:
    > URL Message
    > /NL Hallo Wereld
    > /HI नमस्ते दुनिया
    > /FR Bonjour le monde
    > /ES Hola Mundo
    > /IT Ciao Mondo
    > /CH 你好，世界
    > /JP こんにちは世界
    > /AR مرحبا بالعالم
    > /EN Hello world
        
Example:
    - If you go to /NL your output should be Hallo Wereld; 
    - If you go to /EN your output should be Hello world; 
    - If you go to /FR your output should be Bonjour le monde;

Set-up Steps:
    npm init
    npm install --save express (you might need to do sudo)
    in package.json add the following
        "scripts": {
            "start": "nodemon server.js",
        },
        
Now, when you're code is ready you should be able to use "npm start" to launch your app;
Remember to set the app to listen to localhost:3001.

***Your solution goes to ex01 folder***