const express = require('express');
const app = express ();
const port = 3000;
//const app = require("express")(); -- This is the equivalent of above

app.listen(port, () => {
    console.log(`server running on port ${port}`)
});

/*
app.get("*", (req, res) => {
    res.send(`I catch everything!`)
});
This will render over the other app.gets every time b/c of the "*"
*/

app.get("/", (request, response) => { 
    //Argument of function:
    //request obj = contains information coming from the browser (from the request); 
    //response obj = used to send data back
    response.send(`<h1>Hello from Express!</h1>`)
});

/*
app.get("/about-me/country/hometown", (req, res) => {
    const { person, age } = req.params;
    // var person = req.params.person (is the same as above)
    res.send(`<h1>This is about ${person} page, who is ${age} years old.</h1>`)
});
This has "/about-me" so it will render before the below code, because they have the same wording
*/

app.get("/about-me/:person/:age", (req, res) => { //Person and age are arbitrary, doesn't matter what you write
    const { person, age } = req.params;
    // var person = req.params.person (is the same as above)
    res.send(`<h1>This is about ${person} page, who is ${age} years old.</h1>`)
});

/*
app.get("*", (req, res) => {
    res.send(`404: page not found`);  
})
Placement matters; this only renders if nothing else above did
If you place this at the beginning, this is all that's going to render
Needs to be at the end;
*/
