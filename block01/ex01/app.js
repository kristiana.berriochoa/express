const express = require('express');
const app = express ();
const port = 3001;

app.listen(port, () => {
    console.log(`server running on port ${port}`)
});

app.get("/:language", (req, res) => {
    const { language } = req.params;
    if(language === "NL"){
        res.send(`<h1>Hallo wereld!</h1>`)
    } else if(language === "HI"){
        res.send(`<h1>नमस्ते दुनिया!</h1>`)
    } else if(language === "FR"){
        res.send(`<h1>Bonjour le monde!</h1>`)
    } else if(language === "ES"){
        res.send(`<h1>Hola mundo!</h1>`)
    } else if(language === "IT"){
        res.send(`<h1>Ciao mondo!</h1>`)
    } else if(language === "CH"){
        res.send(`<h1>你好，世界!</h1>`)
    } else if(language === "JP"){
        res.send(`<h1>こんにちは世界!</h1>`)
    } else if(language === "AR"){
        res.send(`<h1> مرحبا بالعالم! </h1>`)
    } else if(language === "EN"){
        res.send(`<h1>Hello world!</h1>`)
    }
});

app.get("*", (req, res) => { 
    res.send(`<h1>Hello world!</h1>`)
});


/*
app.get("/:language", (req, res) => {
    const { language } = req.params;
    if(language === "NL"){
        res.send(`<h1>Hallo Wereld!</h1>`)
    }
});

app.get("/HI", (req, res) => { 
    res.send(`<h1>नमस्ते दुनिया!</h1>`)
});

app.get("/FR", (req, res) => { 
    res.send(`<h1>Bonjour le monde!</h1>`)
});

app.get("/ES", (req, res) => { 
    res.send(`<h1>Hola Mundo!</h1>`)
});

app.get("/IT", (req, res) => { 
    res.send(`<h1>Ciao Mondo!</h1>`)
});

app.get("/CH", (req, res) => { 
    res.send(`<h1>你好，世界!</h1>`)
});

app.get("/JP", (req, res) => { 
    res.send(`<h1>こんにちは世界!</h1>`)
});

app.get("/AR", (req, res) => { 
    res.send(`<h1> مرحبا بالعالم! </h1>`)
});

app.get("/EN", (req, res) => { 
    res.send(`<h1>Hello world!</h1>`)
});

app.get("*", (req, res) => { 
    res.send(`<h1>Hello world!</h1>`)
});
*/