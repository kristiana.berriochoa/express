const express = require('express');
const app = express ();
const port = 3001;

app.listen(port, () => {
    console.log(`server running on port ${port}`)
});

var languages = {
    NL: "Hallo wereld!",
    HI: "नमस्ते दुनिया!",
    FR: "Bonjour le monde!",
    ES: "Hola mundo!",
    IT: "Ciao mondo!",
    CH: "你好，世界!",
    JP: "こんにちは世界!",
    AR: "مرحبا بالعالم!",
    EN: "Hello world!",
};

app.get('/', (req , res ) => {
    res.send(`<h1>Hello world!</h1>`)
});

app.get("/:language", (req, res) => {
    var lang = req.params.language;
    if(lang in languages){
        res.send(`<h1>${languages[lang]}</h1>`)
    } else {
        res.send(`<h1>Hello world in ${lang} not found!</h1>`)
    }
});

app.get("/add/:language/:phrase", (req, res) => {
    const { language, phrase } = req.params; // New language and new phrase
    if(language in languages){ // Check to see if new language exists in object as a key
        res.send(`<h1>${language} already exists!</h1>`) // If yes, send following message;
    } else { // If not, add language
        languages[language] = phrase; // Add language/phrase to the object
        res.send(`<h1>${language} added with message ${newPhrase}</h1>`) // Send following message
    }
});


/*
app.get("/:language", (req, res) => {
    const { language } = req.params;
    if(language === "NL"){
        res.send(`<h1>Hallo wereld!</h1>`)
    } else if(language === "HI"){
        res.send(`<h1>नमस्ते दुनिया!</h1>`)
    } else if(language === "FR"){
        res.send(`<h1>Bonjour le monde!</h1>`)
    } else if(language === "ES"){
        res.send(`<h1>Hola mundo!</h1>`)
    } else if(language === "IT"){
        res.send(`<h1>Ciao mondo!</h1>`)
    } else if(language === "CH"){
        res.send(`<h1>你好，世界!</h1>`)
    } else if(language === "JP"){
        res.send(`<h1>こんにちは世界!</h1>`)
    } else if(language === "AR"){
        res.send(`<h1> مرحبا بالعالم! </h1>`)
    } else if(language === "EN"){
        res.send(`<h1>Hello world!</h1>`)
    }
});

app.get("*", (req, res) => { 
    res.send(`<h1>Hello world!</h1>`)
});
*/