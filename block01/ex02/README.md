## EXERCISE 2:

Extend the previous exercise and add the possibility to add a new language.

Example of usage:
    > URL Message
    > /DE Hello World in DE not found
    > /DE/Hallo_Welt DE added with message "Hallo Welt"
    > /DE Hallo Welt

Note:
Pay attention to the fact that the URL is encoded to ASCII code so it only accepts most common English charactes and not spaces. 
That is why we use the underscore instead of a space! For more information about the encoding of URL you can visit the W3Schools source.

***Your solution goes to ex02 folder***