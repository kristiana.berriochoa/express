const express = require('express');
const app = express ();
const port = 3001;

app.listen(port, () => {
    console.log(`server running on port ${port}`)
});

var accounts = [];

app.get('/', (req , res ) => {
    res.send(`<h1>Welcome to the Bank! How can we assist you?</h1>`)
});

app.get("/account/new/:accountID/:amount", (req, res) => {
    const { accountID } = req.params;
    var amount = Number(req.params.amount);
    var index = accounts.findIndex(account => account.id == accountID)
    if(index === -1){
        accounts.push({id: accountID, amount: amount})
        res.send(`<h1>Account nr ${accountID} created with ${amount} euros</h1>`)
    } else {
        res.send(`<h1>Account nr ${accountID} already present in database.</h1>`)
    }
});

app.get("/:accountID/withdraw/:amount", (req, res) => {
    const { accountID } = req.params;
    var amount = Number(req.params.amount);
    var index = accounts.findIndex(account => account.id == accountID)
    if(index !== -1){
        accounts[index].amount -= amount;
        res.send(`<h1>${amount} euros taken from account nr ${accountID}</h1>`)
    } else {
        res.send(`<h1>Account not found.</h1>`)
    }
}); 

app.get("/:accountID/deposit/:amount", (req, res) => {
    const { accountID } = req.params;
    var amount = Number(req.params.amount);
    var index = accounts.findIndex(account => account.id == accountID)
    if(index !== -1){
        accounts[index].amount += amount;
        res.send(`<h1>${amount} euros added to account nr ${accountID}</h1>`)
    } else {
        res.send(`<h1>Account not found.</h1>`)
    }
});

app.get("/:accountID/balance", (req, res) => {
    const { accountID } = req.params;
    var index = accounts.findIndex(account => account.id == accountID)
    if(index !== -1){
        var total = accounts[index].amount;
        res.send(`<h1>The balance of account nr ${accountID} is ${total} euros </h1>`)
    } else {
        res.send(`<h1>Account not found.</h1>`)
    }
});

app.get("/:accountID/delete", (req, res) => {
    const { accountID } = req.params;
    var index = accounts.findIndex(account => account.id == accountID)
    if(index !== -1){
        delete accounts[index].id;
        res.send(`<h1>Account nr ${accountID} deleted.</h1>`)
    } else {
        res.send(`<h1>Account not found.</h1>`)
    }
});

app.get('*', (req , res ) => {
    res.send(`<h1>404 resource not found!</h1>`)
});