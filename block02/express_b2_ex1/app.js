const app = require ('express')();
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const port = 3001;

app.listen(port, () => {
    console.log(`listening on port ${port}`)
})

/*
Refrence for future DB setup:
    var categories = [{id: 1, category: "food"}, {id: 2, category: "books"}]
    var products = [{id: 123, name: "banana", catID: 1}, {id: 456, name: "Huck Finn", catID: 2}]
*/

var DB = [
    {category: "movies", products: [{name: "The Ring", price: 3.99, color: "black", description: "horror"}]}, 
    {category: "books"},
    {category: "clothes"},
];

app.post("/category/add", (req, res) => {
    var category = req.body.category;
    var index = DB.findIndex(x => x.category == category);
    if(index === -1){
       DB.push({category, products: []}); //res.send({category, product: [req.body]})
       res.status(200).send(DB);
       //res.send(`<h1>Category ${category} added to DB</h1>`);
    } else {
        res.status(200).send(`<h1>Category ${category} already in DB</h1>`);
    }
});

app.post("/category/delete", (req, res) => {
    var category = req.body.category;
    var index = DB.findIndex(x => x.category == category);
    if(index !== -1){
        DB.splice(index, 1);
        res.status(200).send(DB);
        //res.send(`<h1>Category ${category} deleted from DB</h1>`);
    } else {
        res.status(200).send(`<h1>Category not found.</h1>`);
    }
});

app.post("/category/update", (req, res) => {
    var { category, newCategory } = req.body;
    var index = DB.findIndex(x => x.category == category);
    if(index !== -1){
        DB[index].category = newCategory;
        res.status(200).send(DB);
    } else {
        res.status(200).send(`<h1>Category not found.</h1>`);
    }
});

app.get("/category/categories", (req, res) => {
    var categories = DB.map(ele => ele.category);
    console.log(categories);
    res.status(200).send(categories);
});

app.get("/category/products", (req, res) => {
    res.status(200).send(DB);
});

app.get("/category/:category", (req, res) => {
    var category = req.params.category;
    var index = DB.findIndex(x => x.category == category);
    if(index !== -1){
        res.status(200).send(DB[index]);
    } else {
        res.status(200).send(`<h1>Category not found.</h1>`);
    }
});

app.post("/product/add", (req, res) => {
    var category = req.body.category;
    const { name, price, color, description } = req.body;
    var product = { name: name, price: price, color: color, description: description };
    console.log(product);
    var index = DB.findIndex(x => x.category == category);
    if(index !== -1){
        var indexProduct = DB[index].products.findIndex(x => x.name == product.name);
        if(indexProduct === -1){
            DB[index].products.push(product);
            res.status(200).send(DB);
        } else {
            res.status(200).send(`<h1>Product already added to category.</h1>`);
        };
    } else {
        res.status(200).send(`<h1>Category not found.</h1>`);
    }
});

app.post("/product/delete", (req, res) => {
    var category = req.body.category;
    const { name, price, color, description } = req.body;
    var product = { name: name, price: price, color: color, description: description };
    console.log(product)
    var index = DB.findIndex(x => x.category == category);
    if(index !== -1){
        var indexProduct = DB[index].products.findIndex(x => x.name == product.name);
        if(indexProduct !== -1){
            DB[index].products.splice(indexProduct, 1);
            res.status(200).send(DB);
        } else {
            res.status(200).send(`<h1>Product not found.</h1>`);
        };
    } else {
        res.status(200).send(`<h1>Category not found.</h1>`);
    }
});

app.post("/product/update", (req, res) => {
    const { category, nameUp, colorUp, priceUp, desUp, name, price, color, description } = req.body;
    var product = { name: name, price: price, color: color, description: description };
    var index = DB.findIndex(x => x.category == category);
    if(index !== -1){
        var indexProduct = DB[index].products.findIndex(x => x.name == product.name);
        if(indexProduct !== -1){
            DB[index].products[indexProduct].name = nameUp;
            DB[index].products[indexProduct].price = priceUp;
            DB[index].products[indexProduct].color = colorUp;
            DB[index].products[indexProduct].description = desUp;
            res.send(DB[index]);
        } else {
            res.status(200).send(`<h1>Product not found.</h1>`);
        };
    } else {
        res.status(200).send(`<h1>Category not found.</h1>`);
    }
});